type alfanumerico = string | number;

//string
let cadena: alfanumerico = "anibal rivera";

//number
let numero: number = 12;

//Boleano
let verdadero_falso: boolean = true;

//Any
let cualquiera: any = "hola";
cualquiera = 77;

//Arrays
var lenguajes: Array<string> = ["PHP", "JS", "CSS"];

console.log(numero, cadena);
