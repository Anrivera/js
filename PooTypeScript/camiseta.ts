// Interface
interface CamisetaBase {
	setColor(color: string);
	getColor();
	estampacion();
}

// Decorador
function estampar(logo: string) {
	return function (target: Function) {
		target.prototype.estampacion = function (): void {
			console.log("Camiseta estampada con el logo de: " + logo);
		};
	};
}
// Clase (molde del objeto)
@estampar("Gucci Gang")
class Camiseta implements CamisetaBase {
	// Propiedades (caracteristicas del objeto)
	public color: string;
	public modelo: string;
	public marca: string;
	public talla: string;
	public precio: number;

	// Métodos (funciones o acciones del objeto)
	constructor(color, modelo, marca, talla, precio) {
		this.color = color;
		this.modelo = modelo;
		this.marca = marca;
		this.talla = talla;
		this.precio = precio;
	}

	public setColor(color: string) {
		this.color = color;
	}

	public getColor() {
		return this.color;
	}
}

// Clase hija
class Sudadera extends Camiseta {
	public capucha: boolean;

	public setCapucha(capucha: boolean) {
		this.capucha = capucha;
	}

	public getCapucha(): boolean {
		return this.capucha;
	}
}

var camiseta = new Camiseta("uno", "dos", "tres", "cuatro", 12);
console.log(camiseta.estampar());

var sudadera_nike = new Sudadera("uno", "dos", "tres", "cuatro", 12);
sudadera_nike.setCapucha(true);
console.log(sudadera_nike.getCapucha());
