module Tienda { 
    export class Ropa { 
        constructor(public titulo: string) {
            alert(titulo)
        }
    }

    export class Informatica { 
        constructor(public titulo: string) {
            alert('Tienda de tecnología: ' + titulo)
        }
    }
}

import Informatica = Tienda.Informatica;

let cargar_informatica = new Informatica('Supertienda');

function arranque(lanzar: string) {
	return function (target: Function) {
		target.prototype.lanzamiento = function (): void {
			alert(lanzar);
		};
	};
}

@arranque("Lanzamiento del curso de NodeJS");
class Programa {
	public nombre: string;
	public version: number;

	getNombre() {
		return this.nombre;
	}

	setNombre(nombre: string) {
		this.nombre = nombre;
	}

	getVersion() {
		return this.version;
	}

	setVersion(version: number) {
		this.version = version;
	}
}

var programa = new Programa();
programa.lazamiento();
class EditorVideo extends Programa {
	public timeline: number;

	setTimeline(timeline: number) {
		this.timeline = timeline;
	}

	getTimeline() {
		return this.timeline;
	}

	getAllData(): string {
		return `${this.getNombre()} - ${this.getVersion()} - ${this.getTimeline()}`;
	}
}

var editor = new EditorVideo();
editor.setNombre("Camtasia Studio");
editor.setVersion(8);
editor.setTimeline(4000);

console.log(editor.getAllData());

// Logica del formulario

var programas = [];

function guardar() {
	var programa = new Programa();
	var list = "";

	var nombre = (<HTMLInputElement>(
		document.getElementById("nombre")
	)).nodeValue.toString();

	programa.setNombre(nombre);
	programa.setVersion(1);
	programas.push(programa);

	for (var i = 0; i < programas.length; i++) {
		list = `${list} <li> ${programas[i].getNombre()}</li>`;
	}

	var listado = <HTMLInputElement>document.getElementById("listado");
	listado.innerHTML = list;
}

setInterval((nombre_variable, hola) => {
	console.log("Hola");
}, 1000);

var frutas = ["Manzana", "Naranja", "Platano", "Pera"];

var recorrer = frutas.map(function (fruta) {
	console.log(fruta);
	console.log(fruta.length);
});
