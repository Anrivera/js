interface CocheBase {
	getModelo(): string;
	getVelocidad(): number;
}

class Coche implements CocheBase {
	public color: string;
	public modelo: string;
	public velocidad: number = 0;

	constructor(modelo = null) {
		this.color = "Blanco";
		this.velocidad = 0;

		if (!modelo) this.modelo = "BMW Generico";
		else this.modelo = modelo;
	}

	public getModelo(): string {
		return this.modelo;
	}

	public setModelo(modelo: string) {
		this.modelo = modelo;
	}

	public getColor() {
		return this.color;
	}

	public setColor(color: string) {
		this.color = color;
	}

	public acelerar() {
		this.velocidad++;
	}

	public frenar() {
		this.velocidad--;
	}

	public getVelocidad(): number {
		return this.velocidad;
	}
}

var coche = new Coche("Ranault Clio");
var coche_dos = new Coche();
var coche_tres = new Coche();

coche.setColor("ROJO");
coche.acelerar();
coche.acelerar();
coche.acelerar();

console.log("El modelo del coche 1 es: " + coche.getModelo());
console.log("El color del coche 1 es: " + coche.getColor());
console.log("El velocidad del coche 1 es: " + coche.getVelocidad());
/*
coche_dos.setColor("AZUL");
coche_tres.setColor("VERDE");

console.log("El color del coche coche 2 es: " + coche_dos.getColor());
console.log("El color del coche coche 3 es: " + coche_tres.getColor());
*/
