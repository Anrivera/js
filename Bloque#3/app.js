"use strict";

window.addEventListener("load", init, false);

function init() {
	var nombre = document.getElementById("nombre");
	var apellido = document.getElementById("apellido");
	var edad = document.getElementById("edad");
	var btn = document.getElementById("btn");
	window.addEventListener("submit", enviar, false);
	btn.addEventListener("click", enviar, false);
}

function enviar(e) {
	e.preventDefault();
	console.log(nombre.value);

	if (nombre.value != "" || apellido.value != "" || edad.value != "") {
		var info = document.getElementById("info");
		info.innerHTML = `<strong>Nombre: </strong>${nombre.value} <br> <strong>Apellido: </strong>${apellido.value} <br> <strong>Edad: </strong>${edad.value}`;
	} else alert("Los campos del formulario son obligatorios..");
}
