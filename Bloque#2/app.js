"use strict";

/*
1. Pida 6 número por pantalla y los metes en un array.
2. Mostrar el array entero (todos sus elementos) en el cuerpo de la página y en la consola.
3. Ordenarlo y mostrarlo
4. Invertir su orden y mostrarlo
5. Mostrar cuentos elementos tiene el array
6. Busqueda de un valor introducido por el usuario, que nos diga si lo encuentra y su indice (Se valorará el uso de funciones)
*/

function mostrarArray(elementos, textCustom = "") {
	document.write(`<h1>Contenido del array ${textCustom}</h1>`);
	document.write("<ul>");

	numeros.forEach((numero, index) => {
		document.write(`<li><strong>${numero}</strong></li>`);
	});

	document.write("</ul>");
}
// 1. Pedir 6 números
var numeros = [];

for (var i = 0; i <= 5; i++) {
	numeros.push(parseInt(prompt("Introduzca un número", 0)));
}

// 2. Mostrar en el cuerpo de la página
mostrarArray(numeros);

// 3. Ordenar y mostrar
numeros.sort(function (a, b) {
	return a - b;
});

mostrarArray(numeros, "ordenados");

//4. Invertir y mostrar
numeros.reverse();
mostrarArray(numeros, "revertidos");

// 5. Contar elementos
document.write(`<h1>El array tiene: ${numeros.length} elementos`);

//6. Busqueda
var busqueda = parseInt(prompt("Busca un número", 0));
var posicion = numeros.findIndex((numero) => numero == busqueda);

if (posicion && posicion != -1) {
	document.write(`<h1>ENCONTRADO</h1>`);
	document.write(`<h1>Posición de la busqueda: ${posicion}</h1>`);
} else document.write(`<h1>NO ENCONTRADO</h1>`);
